import MySQLdb

conn=MySQLdb.connect(host='localhost',user='root',passwd='root12@#')
cursor = conn.cursor()
cursor.execute("insert into trinc.entity_type_master(entity_type) values('%s')"%('Public Ltd.'))
cursor.execute("insert into trinc.entity_type_master(entity_type) value('%s')"%('Private Ltd.'))
cursor.execute("insert into trinc.entity_type_master(entity_type) values('%s')"%('LLP'))
cursor.execute("insert into trinc.entity_type_master(entity_type) values('%s')"%('Partnership Firm'))
cursor.execute("insert into trinc.entity_type_master(entity_type) values('%s')"%('Proprietor'))
cursor.execute("insert into trinc.entity_type_master(entity_type) values('%s')"%('Assosiation of Firm'))
cursor.execute("insert into trinc.entity_type_master(entity_type) values('%s')"%('Trust'))
cursor.execute("insert into trinc.entity_type_master(entity_type) values('%s')"%('HUF'))
cursor.execute("insert into trinc.entity_type_master(entity_type) values('%s')"%('Individual'))
cursor.execute("insert into trinc.entity_type_master(entity_type) values('%s')"%('Foreign Entity'))


cursor.execute("insert into trinc.business_type_master(business_type) values('%s')"%('CA Firm'))
cursor.execute("insert into trinc.business_type_master(business_type) values('%s')"%('CS Firm'))
cursor.execute("insert into trinc.business_type_master(business_type) values('%s')"%('CWA Firm'))
cursor.execute("insert into trinc.business_type_master(business_type) values('%s')"%('Others'))

cursor.execute("insert into trinc.role_type_master(role_type) values('%s')"%('System Admin'))
cursor.execute("insert into trinc.role_type_master(role_type) values('%s')"%('Business Admin'))
cursor.execute("insert into trinc.role_type_master(role_type) values('%s')"%('Organization Admin'))
cursor.execute("insert into trinc.role_type_master(role_type) values('%s')"%('Organization Employee'))
cursor.execute("insert into trinc.role_type_master(role_type) values('%s')"%('Organization Client'))

cursor.execute("insert into trinc.document_category_master(document_category) values('%s')"%('Permanent'))
cursor.execute("insert into trinc.document_category_master(document_category) values('%s')"%('Current'))
cursor.execute("insert into trinc.document_category_master(document_category) values('%s')"%('Live'))

cursor.execute("insert into trinc.user_type_master(user_type) values('%s')"%('Entity'))
cursor.execute("insert into trinc.user_type_master(user_type) values('%s')"%('Individuals'))
cursor.execute("insert into trinc.user_type_master(user_type) values('%s')"%('Cutomers'))
cursor.execute("insert into trinc.user_type_master(user_type) values('%s')"%('Vendors'))
cursor.execute("insert into trinc.user_type_master(user_type) values('%s')"%('Employees'))
cursor.execute("insert into trinc.user_type_master(user_type) values('%s')"%('Departments'))

cursor.execute("insert into trinc.org_master(org_name) values('%s')"%('Arun Infotech'))
cursor.execute("insert into trinc.org_master(org_name) values('%s')"%('Faralenz Labs'))

cursor.execute("insert into trinc.org_master(org_type_of_entity) values('%s')"%('1'))

cursor.execute("insert into trinc.document_type_master(document_type) values('%s')"%('PAN Card'))
cursor.execute("insert into trinc.document_type_master(document_type) values('%s')"%('Aadhar Card'))
cursor.execute("insert into trinc.document_type_master(document_type) values('%s')"%('Passport'))
cursor.execute("insert into trinc.document_type_master(document_type) values('%s')"%('Driving Licence'))
cursor.execute("insert into trinc.document_type_master(document_type) values('%s')"%('Voter ID Card'))
cursor.execute("insert into trinc.document_type_master(document_type) values('%s')"%('GST Registration Certificate'))
cursor.execute("insert into trinc.document_type_master(document_type) values('%s')"%('Service Tax Registration Certificate'))
cursor.execute("insert into trinc.document_type_master(document_type) values('%s')"%('VAT Registration Certificate'))
cursor.execute("insert into trinc.document_type_master(document_type) values('%s')"%('Certificate of Incorporation'))
cursor.execute("insert into trinc.document_type_master(document_type) values('%s')"%('Provident Fund Registration'))
cursor.execute("insert into trinc.document_type_master(document_type) values('%s')"%('Labour Licence'))
cursor.execute("insert into trinc.document_type_master(document_type) values('%s')"%('IEC Code'))

cursor.execute("insert into trinc.user_master(org_id,role_type_id,user_type_id,user_username,user_password) values(%d,%d,%d,'%s','%s')"%(1,1,1,'superadmin','1234'))
cursor.execute("insert into trinc.user_master(org_id,role_type_id,user_type_id,user_username,user_password) values(%d,%d,%d,'%s','%s')"%(1,2,1,'badmin','1234'))
cursor.execute("insert into trinc.user_master(org_id,role_type_id,user_type_id,user_username,user_password) values(%d,%d,%d,'%s','%s')"%(2,3,2,'cadmin','1234'))
cursor.execute("insert into trinc.user_master(org_id,role_type_id,user_type_id,user_username,user_password) values(%d,%d,%d,'%s','%s')"%(2,4,2,'cemployee','1234'))
cursor.execute("insert into trinc.user_master(org_id,role_type_id,user_type_id,user_username,user_password) values(%d,%d,%d,'%s','%s')"%(1,5,1,'cutomer','1234'))
cursor.execute("insert into trinc.user_master(org_id,role_type_id,user_type_id,user_username,user_password) values(%d,%d,%d,'%s','%s')"%(1,3,1,'amit','amit'))
cursor.execute("insert into trinc.user_master(org_id,role_type_id,user_type_id,user_username,user_password) values(%d,%d,%d,'%s','%s')"%(2,4,2,'pankaj','pankaj'))
cursor.execute("insert into trinc.user_master(org_id,role_type_id,user_type_id,user_username,user_password) values(%d,%d,%d,'%s','%s')"%(2,5,2,'vicky','vicky'))

cursor.execute("insert into trinc.dt_pancard(document_type_id,document_type,org_id,user_id,dt_pancard_pannum,dt_pancard_panname,dt_pancard_panfathersname,dt_pancard_wardnum,dt_pancard_filepathandname,dt_pancard_remarks) values(%d,'%s',%d,%d,'%s','%s','%s','%s','%s','%s')"%(1,'Pancard',2,1,'12345','Vicky','Sanjay','as12','abc','testing'))
cursor.execute("insert into trinc.dt_pancard(document_type_id,document_type,org_id,user_id,dt_pancard_pannum,dt_pancard_panname,dt_pancard_panfathersname,dt_pancard_wardnum,dt_pancard_filepathandname,dt_pancard_remarks) values(%d,'%s',%d,%d,'%s','%s','%s','%s','%s','%s')"%(2,'Pancard',1,1,'RS234','Amit','Ashok','as13','abcd','testing'))

cursor.execute("insert into trinc.dt_aadhar(document_type_id,document_type,org_id,user_id,dt_aadhar_num,dt_aadhar_name,dt_aadhar_parent_or_spouse_name,dt_aadhar_gender,dt_aadhar_address1,dt_aadhar_address2,dt_aadhar_city,dt_aadhar_state,dt_aadhar_pin,dt_aadhar_filepathandname,dt_aadhar_remarks) values(%d,'%s',%d,%d,%d,'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')"%(2,'Aadharcard',1,1,987645234,'Pritee','Rajaram','Male','Yavat','Pune','Pune','Maharashtra','412214','abc','testing'))
 


conn.commit()