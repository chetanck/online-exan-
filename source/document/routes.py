from flask import Blueprint, render_template, request
from flask_login import login_required
from flask import jsonify
import json


from database import db
from .forms import Create,PanCard,Aadhar
from base.models import document_type_master
from base.models import dt_pancard,dt_aadhar


blueprint = Blueprint(
    'document_blueprint',
    __name__,
    url_prefix='/document',
    template_folder='templates',
    static_folder='static'
)



@blueprint.route('/document', methods=['GET','POST'])
@login_required
def index():
	docList = db.session.query(document_type_master.document_type, document_type_master.document_type_id).all()
	print(docList)
	form = Create(request.form)
	return render_template('document.html', form=form, docList = docList)


@blueprint.route('/panCard', methods=['GET','POST'])
@login_required
def index2():
	form = PanCard(request.form)
	if 'submit' in request.form:
		card_id = str(request.form['document_id'])
		
		rw = db.session.query(dt_pancard).filter_by(document_id=card_id).first()
		rw.dt_pancard_panname = str(request.form['dt_pancard_panname'])
		db.session.commit()
	return render_template('panCard.html', form = form)


@blueprint.route('/_get_data/')
@login_required
def _get_data():
    
    document_id = request.args.get('nm')
    
    rowData = db.session.query(dt_pancard).filter_by(document_id=document_id).first()
    print (rowData.user_id)
    print ("------------")
    rowDict = rowData.__dict__ 
    rowDict.pop('_sa_instance_state', None)
    return json.dumps(rowDict, default=str)
    


@blueprint.route('/aadharCard', methods=['GET','POST'])
@login_required
def index3():
	form = Aadhar(request.form)
	if 'submit' in request.form:
		card_id = str(request.form['document_id'])
		rw = db.session.query(dt_aadhar).filter_by(document_id=card_id).first()
		rw.dt_aadhar_name = str(request.form['dt_aadhar_name'])
		db.session.commit()
	return render_template('aadharCard.html', form = form)

@blueprint.route('/_get_dataA/')
@login_required
def _get_dataA():
    
    document_id = request.args.get('nm')
    
    rowData = db.session.query(dt_aadhar).filter_by(document_id=document_id).first()
    print (rowData)
    rowDict = rowData.__dict__ 
    
    rowDict.pop('_sa_instance_state', None)
    return json.dumps(rowDict, default=str)
