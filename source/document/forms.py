from flask_wtf import FlaskForm
from wtforms import TextField, PasswordField, RadioField, SubmitField, SelectField,DateField,FileField

class Create(FlaskForm):
	id = 'createForm'
	docType = SelectField('Select Document Type', id='doc_type')
	submitBtnId = 'submit'

class Aadhar(FlaskForm):
	id = 'Aadhar'
	
	document_type_id =  TextField('Document Type Id', id='document_type_id' ) 
	document_type = TextField('Document Type', id='document_type')
	document_id = TextField('Document Id', id='document_id')
	org_id = TextField('Organization Id', id='org_id')
	user_id = TextField('User Id', id='user_id')
	dt_aadhar_num =  TextField('Aadhar card Number', id='dt_aadhar_num' )
	dt_aadhar_name =  TextField('Aadhar Name', id='dt_aadhar_name' )
	dt_aadhar_parent_or_spouse_name = TextField('Father or Spouse Name',id='dt_aadhar_parent_or_spouse_name') 
	dt_aadhar_dob = DateField('Date Of Birth',id='dt_aadhar_dob')
	dt_aadhar_gender = RadioField('Gender', choices = [('Male','Male'),('Female','Female')],id='dt_aadhar_gender')
	dt_aadhar_address1 = TextField('Address Line-1',id='dt_aadhar_address1')
	dt_aadhar_address2 = TextField('Address Line-2',id='dt_aadhar_address2')
	dt_aadhar_city = TextField('City',id='dt_aadhar_city')
	dt_aadhar_state = TextField('State',id='dt_aadhar_state')
	dt_aadhar_pin = TextField('Pin Code Number',id='dt_aadhar_pin')
	dt_aadhar_filepathandname = FileField('Upload PDF file',id='dt_aadhar_filepathandname')
	dt_aadhar_remarks = TextField('Remark',id='dt_aadhar_remarks')
	submit="submit"


class PanCard(FlaskForm):
	id = 'PanCard'
	
	document_type_id =  TextField('Document Type Id', id='document_type_id' ) 
	document_type = TextField('Document Type', id='document_type')
	document_id = TextField('Document Id', id='document_id')
	org_id = TextField('Organization Id', id='org_id')
	user_id = TextField('User Id', id='user_id')
	dt_pancard_pannum =  TextField('Pan card Number', id='dt_pancard_pannum' )
	dt_pancard_panname =  TextField('Pan Name', id='dt_pancard_panname' )
	dt_pancard_panfathersname = TextField('Fathers Name',id='dt_pancard_panfathersname') 
	dt_pancard_dob = DateField('Date Of Birth',id='dt_pancard_dob')
	dt_pancard_wardnum = TextField('Ward Number',id='dt_pancard_wardnum')
	dt_pancard_filepathandname = FileField('Upload PDF file ',id='dt_pancard_filepathandname')
	dt_pancard_remarks = TextField('Remarks',id='dt_pancard_remarks')
	submit="submit"
	