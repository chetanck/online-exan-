from flask_login import UserMixin
from sqlalchemy import Column, Integer, String,MetaData,Date,ForeignKey,Boolean
from database import Base


class User(Base, UserMixin):

    __tablename__ = 'user_master'

    id = Column('user_id',Integer, primary_key=True)
    username = Column('username',String, unique=True)
    first_name =Column('first_name',String, unique=True)
    middle_name =Column('middle_name',String, unique=True)
    last_name =Column('last_name',String, unique=True)
    education =Column('education',String, unique=True)
    email = Column( 'email',String, unique=True)
    password = Column('password',String,unique=True)
    confirm_password = Column('confirm_password',String, unique=True)

    def __init__(self, **kwargs):
        for property, value in kwargs.items():
            # depending on whether value is an iterable or not, we must
            # unpack it's value (when **kwargs is request.form, some values
            # will be a 1-element list)
            if hasattr(value, '__iter__') and not isinstance(value, str):
                # the ,= unpack of a singleton fails PEP8 (travis flake8 test)
                value = value[0]
            setattr(self, property, value)









class department_master(Base, UserMixin):

    __tablename__ = 'department_master'

    dept_id = Column('dept_id',Integer, primary_key=True)

    dept_name = Column('dept_name',String(120))

    description = Column('description',String(260))


    def __init__(self, **kwargs):
        for property, value in kwargs.items():
            # depending on whether value is an iterable or not, we must
            # unpack it's value (when **kwargs is request.form, some values
            # will be a 1-element list)
            if hasattr(value, '__iter__') and not isinstance(value, str):
                # the ,= unpack of a singleton fails PEP8 (travis flake8 test)
                value = value[0]
            setattr(self, property, value)

class technologies(Base, UserMixin):

    __tablename__ = 'technologies'

    tech_id = Column('tech_id',Integer, primary_key=True)

    tech_name = Column('tech_name',String(120))

    description = Column('description',String(260))


    def __init__(self, **kwargs):
        for property, value in kwargs.items():
            # depending on whether value is an iterable or not, we must
            # unpack it's value (when **kwargs is request.form, some values
            # will be a 1-element list)
            if hasattr(value, '__iter__') and not isinstance(value, str):
                # the ,= unpack of a singleton fails PEP8 (travis flake8 test)
                value = value[0]
            setattr(self, property, value)


class quetions(Base, UserMixin):

    __tablename__ = 'quetions'

    que_id = Column('que_id',Integer, primary_key=True)

    tech_id = Column('tech_id',Integer, ForeignKey('technologies.tech_id'))

    quetions = Column('tech_name',String(120))


    def __init__(self, **kwargs):
        for property, value in kwargs.items():
            # depending on whether value is an iterable or not, we must
            # unpack it's value (when **kwargs is request.form, some values
            # will be a 1-element list)
            if hasattr(value, '__iter__') and not isinstance(value, str):
                # the ,= unpack of a singleton fails PEP8 (travis flake8 test)
                value = value[0]
            setattr(self, property, value)

   

class options(Base, UserMixin):

    __tablename__ = 'options'

    opt_id = Column('opt_id',Integer, primary_key=True)

    que_id = Column('que_id',Integer, ForeignKey('quetions.que_id'))

    option_name = Column('option_name',String(120))


    def __init__(self, **kwargs):
        for property, value in kwargs.items():
            # depending on whether value is an iterable or not, we must
            # unpack it's value (when **kwargs is request.form, some values
            # will be a 1-element list)
            if hasattr(value, '__iter__') and not isinstance(value, str):
                # the ,= unpack of a singleton fails PEP8 (travis flake8 test)
                value = value[0]
            setattr(self, property, value)


class answer(Base, UserMixin):

    __tablename__ = 'answer'

    ans_id = Column('ans_id',Integer, primary_key=True)

    que_id = Column('que_id',Integer, ForeignKey('quetions.que_id'))

    opt_id = Column('opt_id', Integer, ForeignKey('options.opt_id'))



    def __init__(self, **kwargs):
        for property, value in kwargs.items():
            # depending on whether value is an iterable or not, we must
            # unpack it's value (when **kwargs is request.form, some values
            # will be a 1-element list)
            if hasattr(value, '__iter__') and not isinstance(value, str):
                # the ,= unpack of a singleton fails PEP8 (travis flake8 test)
                value = value[0]
            setattr(self, property, value)


