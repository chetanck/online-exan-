from flask_wtf import FlaskForm
from wtforms import TextField, PasswordField, RadioField,TextAreaField,DateField
from wtforms import validators, ValidationError
from wtforms.fields.html5 import EmailField


class AddUser(FlaskForm):
	id = 'AddUser'
	username= TextField('Username *',[validators.Required("Please enter your name.")], id='username' ) 
	password=PasswordField('Password*',id='password')
	user_first_name=TextField('First name',id='user_first_name')
	user_middle_name=TextField('Middle Name',id='user_middle_name')
	user_last_name=TextField('Last Name',id='user_last_name')
	user_gender=RadioField('Gender', choices = [('Male','Male'),('Female','Female')],id='user_gender')
	user_dob=DateField('Date Of Birth',id='user_dob')
	user_address_line_1=TextAreaField('User Address 1',id='user_address_line_1')
	user_address_line_2=TextAreaField('User Address 2',id='user_address_line_2')
	user_city=TextField('User City',id='user_city')
	user_state=TextField('User State',id='user_state')
	user_pin_code_no=TextField('Pin Code',id='user_pin_code_no')
	user_land_line_number=TextField('Land Line Number',id='user_land_line_number')
	user_mobile_number=TextField('Mobile Number',id='user_mobile_number')
	user_alternative_mobile_number =TextField('Mobile Number(Alternative)',id='user_alternative_mobile_number')
	email=TextField('Email ID *',[validators.Required("Please enter your email address."),validators.Email("Please enter your email address.")],id='user_email_id')
	user_alternative_email_id=TextField('Email ID(Alternative)',[validators.Email("Please enter your email address.")],id='user_alternative_email_id')
	user_remarks=TextField('Remark',id='user_remarks')
	submit = "submit"
		
class EditUser(FlaskForm):
	id = 'EditUser'
	username= TextField('Username', id='username') 
	password=PasswordField('Password',id='password')
	user_first_name=TextField('First name',id='user_first_name')
	user_middle_name=TextField('Middle Name',id='user_middle_name')
	user_last_name=TextField('Last Name',id='user_last_name')
	user_gender=RadioField('Gender', choices = [('Male','Male'),('Female','Female')],id='user_gender')
	user_dob=DateField('Date of Birth',id='user_dob')
	user_address_line_1=TextAreaField('User Address 1',id='user_address_line_1')
	user_address_line_2=TextAreaField('User Address 2',id='user_address_line_2')
	user_city=TextField('User City',id='user_city')
	user_state=TextField('User State',id='user_state')
	user_pin_code_no=TextField('Pin Code',id='user_pin_code_no')
	user_land_line_number=TextField('Land Line Number',id='user_land_line_number')
	user_mobile_number=TextField('Mobile Number',id='user_mobile_number')
	user_alternative_mobile_number =TextField('Mobile Number(Alternative)',id='user_alternative_mobile_number')
	email=TextField('Email ID',[validators.Required("Please enter your email address."),validators.Email("Please enter your email address.")],id='user_email_id')
	user_alternative_email_id=TextField('Email ID(Alternative)',[validators.Email("Please enter your email address.")],id='user_alternative_email_id')
	user_remarks=TextField('Remark',id='user_remarks')
	submit = "submit"
		