from flask import Blueprint, render_template, request
from flask_login import login_required
from flask import jsonify
import json

blueprint = Blueprint(
    'user_blueprint',
    __name__,
    url_prefix='/user',
    template_folder='templates',
    static_folder='static'
)

from database import db
from .forms import AddUser


from base.models import User

@blueprint.route('/user_master', methods=['GET','POST'])
@login_required
def index():
	form = AddUser(request.form)
	if 'submit' in request.form:
		users = User(**request.form)
		print("**************************************************")
		print(users.user_gender)
		db.session.add(users)
		db.session.commit()
	return render_template('user_master.html', form = form)





@blueprint.route('/_get_data/')
@login_required
def _get_data():
    username = request.args.get('nm')
    rowData = db.session.query(User).filter_by(username=username).first()
    rowDict = rowData.__dict__ 
    rowDict.pop('_sa_instance_state', None)
    return json.dumps(rowDict, default=str)
    
