from flask import Blueprint, render_template, request
from flask_login import login_required, current_user
from flask import jsonify
import json


blueprint = Blueprint(
    'organization_blueprint',
    __name__,
    url_prefix='/organization',
    template_folder='templates',
    static_folder='static'
)


from database import db
from .forms import AddOrg
from .forms import EditOrg


from base.models import org_master,entity_type_master,business_type_master



@blueprint.route('/org', methods=['GET','POST'])
@login_required
def index():
    docList = db.session.query(entity_type_master.entity_type, entity_type_master.entity_type_id).all()
    docList2 = db.session.query(business_type_master.business_type, business_type_master.business_type_id).all()
    form = AddOrg(request.form)
    if 'submit' in request.form:        
        org = org_master(**request.form)
        print(org)
        db.session.add(org)
        db.session.commit()
    return render_template('addOrg.html', form = form, docList=docList, docList2=docList2)

@blueprint.route('/edit', methods=['GET','POST'])
@login_required
def index2():
    form = EditOrg(request.form)
    if 'submit' in request.form:
        org_name = str(request.form['org_name'])
        rw = db.session.query(org_master).filter_by(org_name=org_name).first()
        rw.org_address_line_1 = str(request.form['org_address_line_1'])
        db.session.commit()
    return render_template('EditOrg.html', form = form)

@blueprint.route('/_get_data/')
@login_required
def _get_data():
    #print("ABOUT TO GET THE DATA AND RETURNING THE JSON--------------")
    org_name = request.args.get('nm')
    #print(org_name)
    rowData = db.session.query(org_master).filter_by(org_name=org_name).first()
    print("~~~~~~~~~~~~~~~~~~")
    
    rowDict = rowData.__dict__ 
    rowDict.pop('_sa_instance_state', None)
    print(rowDict) 
    entityRow = db.session.query(entity_type_master).filter_by(entity_type_id=rowDict['org_type_of_entity']).first()
    print(entityRow)
    rowDict1 = entityRow.__dict__
    print(rowDict1) 
    rowDict['org_type_of_entity'] = rowDict1['entity_type']
    print("----------------")
    print(rowDict)
    print("----------------")
       
    return json.dumps(rowDict, default=str)
    