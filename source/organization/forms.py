from flask_wtf import FlaskForm
from wtforms import TextField, PasswordField, RadioField, DateField, BooleanField,SelectField
from wtforms import validators, ValidationError
from wtforms.fields.html5 import DateField

class AddOrg(FlaskForm):
	id = 'AddOrg'
	
	org_name =  TextField('Organization Name *', id='org_name' ) 
	org_type_of_entity = SelectField('Organization Type Of entity *', id='org_type_of_entity')
	org_nature_of_business = SelectField('Organization Nature Of Business', id='org_nature_of_business')
	org_company_registration_no = TextField('Organization Company Registration No', id='org_company_registration_no')	
	org_date_of_incorporation = DateField('Organization Date Of Incorporation',id='org_date_of_incorporation')


	org_address_line_1 =  TextField('Organization Address', id='org_address_line_1' )
	org_address_line_2 =  TextField('Organization Address 2', id='org_address_line_2' )
	org_city = TextField('Organization City',id='org_city') 
	org_state = TextField('Organization state',id='org_state')
	org_pin_code_no = TextField('Organization Pin Code No',id='org_pin_code_no')
	org_parmanent_account_number = TextField('Organization Permanent Account Number',id='org_parmanent_account_number')
	org_tax_deduction_account_number = TextField('Organization Tax Deduction Account Number',id='org_tax_deduction_account_number')
	org_goods_service_tax_registration_number = TextField('Organization Goods Service Tax Registration Number',id='org_goods_service_tax_registration_number')
	org_import_export_code = TextField('Organization Import Export Code',id='org_import_export_code')
	org_land_line_number = TextField('Organization Land Line Number',id='org_land_line_number')
	org_mobile_number = TextField('Organization Mobile Number',id='org_mobile_number')
	org_alternative_mobile_number = TextField('Organization Alternative Mobile Number',id='org_alternative_mobile_number')
	org_email_id = TextField('Organization Email Id *',[validators.Required("Please enter your email address."),validators.Email("Please enter your email address.")],id='org_email_id')
	org_alternative_email_id = TextField('Organization Alternative Email Id',[validators.Email("Please enter your email address.")],id='org_alternative_email_id')
	org_remarks = TextField('Organization Remarks',id='org_remarks') 
	submit = "submit"
		
class EditOrg(FlaskForm):
	id = 'EditOrg'
	
	org_name =  TextField('Organization Name', id='org_name' ) 
	org_type_of_entity = TextField('Organization Type Of entity', id='org_type_of_entity')
	org_nature_of_business = TextField('Organization Nature Of Business', id='org_nature_of_business')
	org_company_registration_no = TextField('Organization Company Registration No', id='org_company_registration_no')
	org_date_of_incorporation = DateField('Organization Date Of Incorporation', id='org_date_of_incorporation')
	org_address_line_1 =  TextField('Organization Address', id='org_address_line_1' )
	org_address_line_2 =  TextField('Organization Address 2', id='org_address_line_2' )
	org_city = TextField('Organization City',id='org_city') 
	org_state = TextField('Organization state',id='org_state')
	org_pin_code_no = TextField('Organization Pin Code No',id='org_pin_code_no')
	org_parmanent_account_number = TextField('Organization Permanent Account Number',id='org_parmanent_account_number')
	org_tax_deduction_account_number = TextField('Organization Tax Deduction Account Number',id='org_tax_deduction_account_number')
	org_goods_service_tax_registration_number = TextField('Organization Goods Service Tax Registration Number',id='org_goods_service_tax_registration_number')
	org_import_export_code = TextField('Organization Import Export Code',id='org_import_export_code')
	org_land_line_number = TextField('Organization Land Line Number',id='org_land_line_number')
	org_mobile_number = TextField('Organization Mobile Number',id='org_mobile_number')
	org_alternative_mobile_number = TextField('Organization Alternative Mobile Number',id='org_alternative_mobile_number')
	org_email_id = TextField('Organization Email Id *',[validators.Required("Please enter your email address."),validators.Email("Please enter your email address.")],id='org_email_id')
	org_alternative_email_id = TextField('Organization Alternative Email Id',[validators.Email("Please enter your email address.")],id='org_alternative_email_id')
	org_remarks = TextField('Organization Remarks',id='org_remarks')
	submit = "submit"